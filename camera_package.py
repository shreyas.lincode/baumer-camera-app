import cv2
from kafka import KafkaProducer
import sys
import json
import base64
import tldextract
from camera_settings import *

### This code is for assigning camera index's while selecting workstation

def check_for_new_idxs(new_idx, current_idx):
    common_set = set(new_idx) & set(current_idx)
    new_set = set(new_idx) - common_set
    new_list = list(new_set)
    return new_list

class CameraSelect(Camera):
    def __init__(self, KAFKA_BROKER_URL, topic, camera_id):
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
                             value_serializer=lambda value: json.dumps(value).encode(), max_request_size=41943040000000000000000)
        self.th = None
        self.poll_for_part_id()


        if str(camera_id).find(".") == -1:
            self.cap = cv2.VideoCapture(int(self.cam_id))
            # self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
            # self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
            # self.cap.set(cv2.CAP_PROP_FPS, 25)
            self.is_ip = False
        else:
            self.cap = IPWEBCAM(root_url=str(self.cam_id))
            self.is_ip = True
            print("mobile found")

    def publish_frame_to_kafka(self):
        f_name = str(self.cam_id) + "_tmp.jpg"
        cv2.imwrite(f_name, self.frame)
        with open(f_name, 'rb') as f:
            im_b64 = base64.b64encode(f.read())
        payload_video_frame = {"frame": str(im_b64),"idx":str(self.cam_id)}
        print(self.topic)
        future = self.producer.send(self.topic, value=payload_video_frame)
        report = future.get()


global domain
ws_name = sys.argv[1]
name = sys.argv[1]
email = sys.argv[2]
domain = tldextract.extract(email).domain
current_idx = []
new_idx = []

while True:

    for i in range(10):
        try:
            cap = cv2.VideoCapture(i)
            ret,frame = cap.read()
            cv2.imwrite(str(i)+'.jpg',frame)
            print('Port :', str(i),' is connected!!')
            new_idx.append(str(i))
        except:
            pass

    ### update camera indices to mongo on restart
    mp = MongoHelper().getCollection(WORKSTATION_COLLECTION, domain=domain)
    coll = mp.find_one({"workstation_name": str(ws_name)})
    coll["restart"] = True
    coll["available_indexs"] = str(new_idx)
    wid = coll["_id"]
    mp.update({'_id' : coll['_id']}, {'$set' :  coll})

    base_topic = str(wid)
    print("\ntopic "+base_topic)
    print("\ncamera index is "+str(new_idx))

    ## check if any new idxs are present
    new_list = check_for_new_idxs(new_idx, current_idx)

    print("\n new list is"+str(new_list))

    for i in new_list:
        topic = base_topic + "_" + str(i) + "_i"
        cc = CameraSelect(KAFKA_BROKER_URL, topic, i)
        cc.start()

    current_idx = []
    current_idx = new_idx
    new_idx = []



