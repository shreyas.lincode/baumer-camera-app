from ipaddress import ip_address
import urllib
import cv2
import numpy as np
import urllib.request
import time
import base64
import json
from kafka import KafkaProducer
import threading
from PIL import Image, ImageEnhance
import numpy
from pymongo import MongoClient
from abc import ABC, abstractmethod
import sys
import tldextract
import neoapi


KAFKA_BROKER_URL = "34.93.101.247:9092"
MONGO_SERVER_HOST = "34.93.101.247"
MONGO_SERVER_PORT = 26000
MONGO_DB = "LIVIS"
WORKSTATION_COLLECTION = 'workstations'
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}


def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=MONGO_SERVER_HOST, port=MONGO_SERVER_PORT)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None, domain="lincode"):
        
        _DB = MONGO_DB
        DB = self.client[_DB]
        #make it as cname in [LIST OF COMMON COLLECTIONS]
        if cname == 'permissions':
            pass
        else:
            cname = domain + cname
        #print('collection_name for this request-------' ,cname )
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


## Parent Class
class Camera(ABC):
    #def __init__(self, KAFKA_BROKER_URL, topic, camera_id):
        # self.tfms = {}
        # self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        # self.topic = topic
        # self.cam_id = camera_id
        # self.frame = None
        # self.part_id = None
        # self.killed = False
        # self.thread = None
        # self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
        #                               value_serializer=lambda value: json.dumps(value).encode(), )
        # self.th = None
        # self.poll_for_part_id()
    def _poll_for_part_id(self):
        mp = MongoHelper().getCollection("cam_to_part", domain=domain)
        while True:
            # pr = mp.find_one({'camera_id' : int(self.cam_id)})
            if self.cam_id.find(":") != -1:
                pr = mp.find_one({'camera_id': self.cam_id})
            else:
                pr = mp.find_one({'camera_id': int(self.cam_id)})
            if pr:
                # print('found_part : : : ' , pr)
                self.part_id = pr['part_id']

    def poll_for_part_id(self):
        self.th = threading.Thread(target=self._poll_for_part_id)
        self.th.start()

    @abstractmethod
    def _start(self):
        pass

    def publish_frame_to_kafka(self):
        # tmp_name = self.cam_id
        # tmp_name = tmp_name.replace(":", "")
        # tmp_name = tmp_name.replace("//", "")
        # tmp_name = tmp_name.replace(".", "")
        # f_name = str(tmp_name) + "_tmp.jpg"
        # cv2.imwrite(f_name, self.frame)
        # with open(f_name, 'rb') as f:
        #     im_b64 = base64.b64encode(f.read())
        # payload_video_frame = {"frame": str(im_b64)}
        payload_video_frame = cv2.imencode('.jpeg', self.frame)[1].tobytes()
        print("Topic is")
        print(self.topic)
        future = self.producer.send(self.topic, value=payload_video_frame)
        report = future.get()

    def get_tfms(self):
        self.tfms = {}
        if self.part_id:
            mp = MongoHelper().getCollection(self.part_id + "_preprocessingpolicy", domain=domain)
            pre = mp.find_one({'camera_id': str(self.cam_id)})
            p = pre['policy']
            for key, value in p.items():
                if key in ['brightness', 'contrast', 'hue', 'saturation']:
                    self.tfms[key] = value
                    # print(self.tfms[key])
        return self.tfms

    def apply_transform(self):
        if self.tfms:
            for k, val in self.tfms.items():
                val = float(val)
                print('applying tfms {}'.format(k))
                if k == 'brightness':
                    if val != 0:
                        val = val * 100
                        hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
                        h, s, v = cv2.split(hsv)
                        v = cv2.add(v, val)
                        v[v > 255] = 255
                        v[v < 0] = 0
                        final_hsv = cv2.merge((h, s, v))
                        self.frame = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
                    else:
                        self.frame = self.frame
                if k == 'contrast':
                    if val != 0:
                        val = val * 3
                        print("val is:", val)
                        ff = Image.fromarray(np.uint8(self.frame)).convert('RGB')
                        f = ImageEnhance.Contrast(ff)
                        e_img = f.enhance(val)
                        self.frame = numpy.asarray(e_img)
                    else:
                        self.frame = self.frame

    def start(self):
        self.thread = threading.Thread(target=self._start)
        self.thread.start()

    def stop(self):
        self.killed = True
        time.sleep(0.2)
        self.thread.join()
        self.thread = None
        self.killed = False


class IPWEBCAM(Camera):
    def __init__(self,  KAFKA_BROKER_URL, topic, camera_id, width=400, height=400):
        #self.url = 'http://' + root_url
        self.width = width
        self.height = height
        self.resolutions = {
            "0": "1920x1080",
            "1": "1280x720",
            "2": "960x720",
            "3": "720x480",
            "4": "640x480",
            "5": "352x288",
            "6": "320x240",
            "7": "256x144",
            "8": "176x144"
        }
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = 'http://' + camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
                                    #   value_serializer=lambda value: json.dumps(value).encode(), max_request_size=41943040000000000000000)
        self.th = None
        self.poll_for_part_id()

    def get_image(self):
        # Get our image from the phone
        imgResp = urllib.request.urlopen(self.cam_id + '/shot.jpg')
        # Convert our image to a numpy array so that we can work with it
        imgNp = np.array(bytearray(imgResp.read()), dtype=np.uint8)
        # Convert our image again but this time to opencv format
        img = cv2.imdecode(imgNp, -1)
        img = cv2.resize(img,(640,640))
        return img

    def _start(self):
        while not self.killed:
            try:
                    self.frame = self.get_image()
            except:
                    continue
            if self.frame is None:
                # print("None Frame Recieved!")
                continue
            #### check tfms and apply here
            if self.part_id:
                self.tfms = self.get_tfms()
                if self.tfms:
                    print(self.tfms)
                    self.apply_transform()
            ####
            self.publish_frame_to_kafka()


class USBCamera(Camera):
    def __init__(self, KAFKA_BROKER_URL, topic, camera_id):
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
                                    #   value_serializer=lambda value: json.dumps(value).encode(), max_request_size=41943040000000000000000)
        self.th = None
        self.poll_for_part_id()
        self.cap = cv2.VideoCapture(int(self.cam_id))

    def _start(self):
        while not self.killed:
            try:
                ret, self.frame = self.cap.read()
            except:
                continue
            if self.frame is None:
                # print("None Frame Recieved!")
                continue
            #### check tfms and apply here
            if self.part_id:
                self.tfms = self.get_tfms()
                if self.tfms:
                    print(self.tfms)
                    self.apply_transform()
            ####
            self.publish_frame_to_kafka()


class Baumer(Camera):
    def __init__(self, KAFKA_BROKER_URL, topic, camera_id, ip_address):
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = camera_id  #"192.168.0.1"
        self.frame = None
        self.ip_address = ip_address
        self.part_id = Nonexzzz
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
                                      value_serializer=lambda value: json.dumps(value).encode(), )
        self.th = None
        self.poll_for_part_id()
        self.cap = neoapi.Cam()
        self.cap.Connect(camera_id)
        self.cap.f.ExposureAuto.SetString('Off')
        # self.cap.f.ExposureTime.Set(300000)
        self.cap.f.PixelFormat.SetString('BayerRG8')
        # time.sleep(2)
        # width = 1280
        # height = 720

        def _start(self):
            while not self.killed:
                try:
                    img = self.cap.GetImage().GetNPArray()
                    img = cv2.cvtColor(img,cv2.COLOR_BAYER_RG2RGB)
                except:
                    continue
                if self.frame is None:
                    # print("None Frame Recieved!")
                    continue
                #### check tfms and apply here
                if self.part_id:
                    self.tfms = self.get_tfms()
                    if self.tfms:
                        print(self.tfms)
                        self.apply_transform()
                ####
                self.publish_frame_to_kafka()


def run(name, email):
    global domain
    #name = sys.argv[1]
    #print(name)
    # email = sys.argv[2]
    domain = tldextract.extract(email).domain
    dict_of_topics = {}
    mp = MongoHelper().getCollection(WORKSTATION_COLLECTION, domain=domain)

    workstations = [p for p in mp.find({"$and": [{"isdeleted": False}, {"isdeleted": {"$exists": True}},
                                                {"workstation_name":name}]})]

    print("Workstation FOUND : ", len(workstations))

    if workstations:
        for w_s in workstations:
            cameras = w_s['cameras']
            wids = w_s['_id']
            ip_address = w_s["camera_id"]
            for c in cameras:
                camera_ids = c['camera_id']
                topic = str(wids) + "_" + str(camera_ids) + "_i"
                topic = topic.replace(":", "-")
                dict_of_topics[topic] = c['camera_type']
                topic = ""

    print(dict_of_topics)
    for key,value in dict_of_topics.items():
        topic = key
        print(topic.split('_'))
        camera_id = str(topic.split('_')[1])
        if value == "IP":
            camera_id = camera_id.replace("-",":")
            topic = topic.replace("-", "")
            topic = topic.replace(".","")
            cc = IPWEBCAM(KAFKA_BROKER_URL, topic, camera_id)
        if value == "USB":
            cc = USBCamera(KAFKA_BROKER_URL, topic, camera_id)
        if value == "Gige-Baumer":
            cc = Baumer(KAFKA_BROKER_URL, topic, camera_id, ip_address)
        cc.start()


run("deepanshu_cam", "divya.sai@livis.com")
