from tkinter import *
import urllib
import cv2
import numpy as np
import urllib.request
import time
import json
from kafka import KafkaProducer, KafkaConsumer
import threading
from PIL import Image, ImageEnhance
import numpy
from pymongo import MongoClient
from abc import ABC, abstractmethod
import tldextract
import subprocess
import threading
# from arena_api.system import system
# from arena_api.buffer import *
# import plc_module
# from Button import Button
# import status_module
import sys
# import neoapi
import requests
import redis 
import argparse
from cam_service_for_ws_PLC import *
# Instantiate the parser
# print("Started!!")
parser = argparse.ArgumentParser(description='Optional app description')
# print("HELLO!!!!")

# KAFKA_BROKER_URL = "127.0.0.1:9092"
KAFKA_BROKER_URL = "34.81.179.201:9092"
MONGO_SERVER_HOST = "3.110.112.117"
MONGO_SERVER_PORT = 7778
MONGO_DB = "LIVIS"
WORKSTATION_COLLECTION = 'workstations'
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}
wids = ""
camera_ids = ""


def ping_check(ip):

    cmd = ['ping', str(ip), '-c', '1']
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    # Linux Version p = subprocess.Popen(['ping','127.0.0.1','-c','1',"-W","2"])
    # The -c means that the ping will stop afer 1 package is replied
    # and the -W 2 is the timelimit
    if "Destination Host Unreachable" in output.decode('utf-8'):
        print("IP not connected")
        return False
    else:
        print("PING successful!!!!!!")
        return True


def run(ws_name, email):
    global domain
    global wids
    global camera_ids
    if ws_name == "" or email == "" :
        print("Please Enter all values")
        return
    print(">>>>>>>>>>>",ws_name, email)
    domain = tldextract.extract(email).domain
    print(domain)
    dict_of_topics = {}
    mp = MongoHelper().getCollection(WORKSTATION_COLLECTION, domain=domain)

    workstations = [p for p in mp.find({"$and": [{"isdeleted": False}, {"isdeleted": {"$exists": True}},
                                                 {"workstation_name": ws_name}]})]
    # print(WORKSTATION_COLLECTION, domain)
    print("Workstation FOUND : ", len(workstations))

    if workstations:
        for w_s in workstations:
            print(w_s)

            cameras = w_s['cameras']
            wids = w_s['_id']
            for c in cameras:
                camera_ids = c['camera_id']
                topic = str(wids) + "_" + str(camera_ids) + "_i"
                topic = topic.replace(":", "-")
                dict_of_topics[topic] = c['camera_type']
                topic = ""

    for key, value in dict_of_topics.items():
        topic = key
        print(topic.split('_'))
        camera_id = str(topic.split('_')[1])
        value = "Gige - Baumer"
        if value == "Gige - Baumer" or value == "Lucid":
            # cc = Baumer(KAFKA_BROKER_URL, topic, camera_id)
            cc = StreamBaumerFromRedis(KAFKA_BROKER_URL, topic, camera_id)
            # cc.connect()
        cc.start()
        # t1 = threading.Thread(target=cc.start)
        # t1.start()

def run_baumer_to_redis():
    #Write this outside
    os.system("python baumer_to_redis.py")


def LoginPage():
    login_screen=Tk()
    login_screen.title("LIVIS Workstation")
    login_screen.geometry("1028x418")
    # Add image file
    bg = PhotoImage( file = "bg.png")
    # Show image using label
    label1 = Label( login_screen, image = bg)
    label1.place(x = 0,y = 0)
  
    welcome_text = Label(login_screen, text="Please enter login details")
    welcome_text.place(relx=0.1,rely=0.12)
    
    
    #Label(login_screen, text="").pack()
    username_icon = Label(login_screen, text="User ID")
    username_icon.place(relx=0.1,rely=0.20)
    username_login_entry = Entry(login_screen, textvariable="username")
    username_login_entry.place(relx=0.2,rely=0.20)
    #username_login_entry.pack()
    #Label(login_screen, text="").pack()
    password_icon = Label(login_screen, text="WS Name")
    password_icon.place(relx=0.1,rely=0.30)
    password__login_entry = Entry(login_screen, textvariable="password")
    password__login_entry.place(relx= 0.2, rely=0.30)
    #password__login_entry.pack()
    #Label(login_screen, text="").pack()
    #cameraip_icon = Label(login_screen, text="CameraIP")
    #cameraip_icon.place(relx=0.1,rely=0.40)
    #cameraip__login_entry = Entry(login_screen, textvariable="cameraIP")
    #cameraip__login_entry.place(relx= 0.2, rely=0.40)
    license_text = open('license.txt','r').read().strip()
    display_license = "License Key     " + license_text 
    license_icon = Label(login_screen, text=display_license)
    license_icon.place(relx=0.1,rely=0.40)

    license_validity_icon = Label(login_screen, text="License Valid   01/01/2022 - 31/12/2022" )
    license_validity_icon.place(relx=0.1,rely=0.50)

    submit_button = Button(login_screen, text="START", width=10, height=3, command=lambda : run(password__login_entry.get(), username_login_entry.get()))
    submit_button.place(relx= 0.18, rely=0.6)
    login_screen.mainloop()
LoginPage()
