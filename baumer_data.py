import sys
import cv2
import neoapi
import time
import datetime

# Sai's script for Baumer camera streaming.


camera = neoapi.Cam()
print(camera.Connect("192.168.0.1"))
camera.f.ExposureAuto.SetString('Off')
camera.f.ExposureTime.Set(300000)
camera.f.PixelFormat.SetString('BGR8')
# print(dir())
# camera.f.AutoFeatureOffsetX.Set(10)
# camera.f.AutoFeatureOffsetY.Set(5)
time.sleep(2)
width = 1280
height = 720
start=datetime.datetime.now()
# i=0
# camera.f.ExposureTime.Set(i*100000)
while True:
	# print('start')
	
	# print(camera.f.ExposureTime.Get())
	img = camera.GetImage().GetNPArray()
	# print(len(img))
	if len(img)==0:
		continue
	# else:
	# 	camera.f.ExposureTime.Set(i*100000)
	# 	print(camera.f.ExposureTime.Get())
	# img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	# img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
	# print(img.shape)
	img = cv2.resize(img,(width,height))
	stop = datetime.datetime.now()
	print(stop-start)
	# start=datetime.datetime.now()
	# img = camera.GetImage()
	# print(dir(camera.GetImage()))
	# print(img)
	# print(img.shape)
	title = 'Press [ESC] to exit ..'
	# cv2.namedWindow(title, cv2.WINDOW_NORMAL)
	# cv2.resizeWindow(title, width, height)
	cv2.imshow(title, img)
	# cv2.waitKey(1)
	# print('stop')
	# continue
	# if cv2.waitKey(1):
	# 	break	
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	# print('stop1')
	# i+=1
	start=datetime.datetime.now()

cv2.destroyAllWindows()
