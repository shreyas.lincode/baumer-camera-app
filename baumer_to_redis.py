import cv2
import neoapi
import redis
import numpy as np 


class BaumerCamera:

    def __init__(self, key) -> None:
        self.camera = neoapi.Cam()
        self.camera.Connect()
        self.camera.f.PixelFormat.SetString("BayerRG8")
        self.redis_db = redis.StrictRedis(host="localhost", port=6379,db=0)
        self.imgarray = self.camera.GetImage.GetNPArray()
        self.key = key

    def encode_frames(self):
        count =  0
        while True:
            # img = self.camera.GetImage()
            if not self.imgarray.IsEmpty():
                
                imgarray = cv2.cvtColor(imgarray, cv2.COLOR_BAYER_RG2RGB)
                _, buffer = cv2.imencode('.png', imgarray)
                img1_bytes = np.array(buffer).tobytes()
                self.redis_db.set(self.key, img1_bytes)
                print(f"encoding {count}")
                count += 1

class RedisDB:

    def __init__(self) -> None:
        self.redis_db = redis.StrictRedis(host="localhost", port=6379,db=0)
             
   

camera = BaumerCamera('frames')

while True:
    camera.encode_frames()


