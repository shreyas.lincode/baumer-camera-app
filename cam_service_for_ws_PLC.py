import urllib
import cv2
import numpy as np
import urllib.request
import time
import json
from kafka import KafkaProducer, KafkaConsumer
import threading
from PIL import Image, ImageEnhance
import numpy
from pymongo import MongoClient
from abc import ABC, abstractmethod
import tldextract
import subprocess
import threading
import neoapi
import requests
import argparse
import redis
from arena_api.system import system
from arena_api.buffer import *
from arena_api import enums

parser = argparse.ArgumentParser(description='Optional app description')

# KAFKA_BROKER_URL = "127.0.0.1:9092"
# KAFKA_BROKER_URL = "34.81.179.201:9092" # dev-full-load-1
# KAFKA_BROKER_URL = "35.236.186.201:9092" # dev-machine 
KAFKA_BROKER_URL = "34.81.179.201:9092" # dev-machine 
MONGO_SERVER_HOST = "3.110.112.117" # dev-full-load-1
# MONGO_SERVER_HOST = "35.236.186.201" # dev-machine
MONGO_SERVER_PORT = 7778
MONGO_DB = "LIVIS"
WORKSTATION_COLLECTION = 'workstations'
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}
wids = ""
camera_ids = ""

def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class MongoHelper:
    client = None

    def __init__(self):
        if not self.client:
            self.client = MongoClient(
                host=MONGO_SERVER_HOST, port=MONGO_SERVER_PORT)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None, domain="lincode"):

        _DB = MONGO_DB
        DB = self.client[_DB]
        # make it as cname in [LIST OF COMMON COLLECTIONS]
        if cname == 'permissions':
            pass
        else:
            cname = domain + cname
        #print('collection_name for this request-------' ,cname )
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


# Parent Class
class Camera(ABC):
    # def __init__(self, KAFKA_BROKER_URL, topic, camera_id):
    # self.tfms = {}
    # self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
    # self.topic = topic
    # self.cam_id = camera_id
    # self.frame = None
    # self.part_id = None
    # self.killed = False
    # self.thread = None
    # self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
    #                               value_serializer=lambda value: json.dumps(value).encode(), )
    # self.th = None
    # self.poll_for_part_id()
    def _poll_for_part_id(self):
        mp = MongoHelper().getCollection("cam_to_part", domain=domain)
        while True:
            # pr = mp.find_one({'camera_id' : int(self.cam_id)})
            if self.cam_id.find(":") != -1:
                pr = mp.find_one({'camera_id': self.cam_id})
            else:
                pr = mp.find_one({'camera_id': int(self.cam_id)})
            if pr:
                # print('found_part : : : ' , pr)
                self.part_id = pr['part_id']

    def poll_for_part_id(self):
        self.th = threading.Thread(target=self._poll_for_part_id)
        self.th.start()

    @abstractmethod
    def _start(self):
        pass

    def publish_frame_to_kafka(self):
        print("sending to kafka ----------------------->>>>>>")
        print(self.topic)
        payload_video_frame = cv2.imencode('.jpeg', self.frame)[1].tobytes()
        producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
        self.topic = self.topic.replace(".", "")
        self.topic = self.topic.replace(":", "")
        future = producer.send(self.topic, value=payload_video_frame)


    def get_tfms(self):
        self.tfms = {}
        if self.part_id:
            mp = MongoHelper().getCollection(
                self.part_id + "_preprocessingpolicy", domain=domain)
            pre = mp.find_one({'camera_id': str(self.cam_id)})
            p = pre['policy']
            for key, value in p.items():
                if key in ['brightness', 'contrast', 'hue', 'saturation']:
                    self.tfms[key] = value
        return self.tfms

    def apply_transform(self):
        if self.tfms:
            for k, val in self.tfms.items():
                val = float(val)
                print('applying tfms {}'.format(k))
                if k == 'brightness':
                    if val != 0:
                        val = val * 100
                        hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
                        h, s, v = cv2.split(hsv)
                        v = cv2.add(v, val)
                        v[v > 255] = 255
                        v[v < 0] = 0
                        final_hsv = cv2.merge((h, s, v))
                        self.frame = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
                    else:
                        self.frame = self.frame
                if k == 'contrast':
                    if val != 0:
                        val = val * 3
                        print("val is:", val)
                        ff = Image.fromarray(
                            np.uint8(self.frame)).convert('RGB')
                        f = ImageEnhance.Contrast(ff)
                        e_img = f.enhance(val)
                        self.frame = numpy.asarray(e_img)
                    else:
                        self.frame = self.frame

    def start(self):
        self.thread = threading.Thread(target=self._start)
        self.thread.start()

    def stop(self):
        self.killed = True
        time.sleep(0.2)
        self.thread.join()
        self.thread = None
        self.killed = False


def start_process_api(wid, part_id, part_number):
    url = "https://dev.livis.ai/api/livis/v1/inspection/start_process/"



    payload = json.dumps({
    "workstation_id": str(wid),
    "user_id": "a0a746df-6afe-4831-b6fa-d5c1ab5b6bf4",
    "part_id": str(part_id),
    "part_number": part_number,
    "part_description": ""
    })
    headers = {
    'Authorization': 'Token d1fe7eed4bc2ccbf2bdb716e4bb7760cf14f9419',
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return response.text, response.status_code

def end_process_api(inspection_id):

    url = "https://dev.livis.ai/api/livis/v1/inspection/end_process/"

    payload = json.dumps({"inspection_id": inspection_id})

    headers = {
    'Authorization': 'Token d1fe7eed4bc2ccbf2bdb716e4bb7760cf14f9419',
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

def save_images(camera_ids, wids, part_id):
    url = "https://dev.livis.ai/api/livis/v1/capture/capture_image/"

    payload = json.dumps({
    "camera_id": str(camera_ids),
    "part_id": str(part_id),
    "workstation_id": str(wids),
    "golden_image": False
    })


    headers = {
    'Authorization': 'Token d1fe7eed4bc2ccbf2bdb716e4bb7760cf14f9419',
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    # print(response.text)
    return json.loads(response.text)

def inspect_ui_trigger(camera_ids, wids, part_id):


    url = "https://dev.livis.ai/api/livis/v1/inspection/inspect_ui_trigger/"

    payload = json.dumps({
    "camera_id": str(camera_ids),
    "workstation_id": str(wids),
    "part_id": str(part_id)
    })
    headers = {
    'Authorization': 'Token d1fe7eed4bc2ccbf2bdb716e4bb7760cf14f9419',
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    # print(json.loads(response.text)["message"]["is_Accepted"])
    return json.loads(response.text)["message"]["is_Accepted"]

class IPWEBCAM(Camera):
    def __init__(self,  KAFKA_BROKER_URL, topic, camera_id, width=400, height=400):
        self.width = width
        self.height = height
        self.resolutions = {
            "0": "1920x1080",
            "1": "1280x720",
            "2": "960x720",
            "3": "720x480",
            "4": "640x480",
            "5": "352x288",
            "6": "320x240",
            "7": "256x144",
            "8": "176x144"
        }
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = 'http://' + camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
        self.th = None
        self.poll_for_part_id()

    def get_image(self):
        imgResp = urllib.request.urlopen(self.cam_id + '/shot.jpg')
        imgNp = np.array(bytearray(imgResp.read()), dtype=np.uint8)
        img = cv2.imdecode(imgNp, -1)
        # img = cv2.resize(img, (640, 640))
        # cv2.imshow("window", img)
        return img

    def _start(self):
        while not self.killed:
            try:
                self.frame = self.get_image()
            except:
                continue
            if self.frame is None:
                # print("None Frame Recieved!")
                continue
            # check tfms and apply here
            if self.part_id:
                self.tfms = self.get_tfms()
                if self.tfms:
                    print(self.tfms)
                    self.apply_transform()
            ####
            self.publish_frame_to_kafka()


class Baumer(Camera):
    def __init__(self,  KAFKA_BROKER_URL, topic, camera_id, width=400, height=400):
        #self.url = 'http://' + root_url
        self.width = width
        self.height = height
        # self.resolutions = {
        #     "0": "1920x1080",
        #     "1": "1280x720",
        #     "2": "960x720",
        #     "3": "720x480",
        #     "4": "640x480",
        #     "5": "352x288",
        #     "6": "320x240",
        #     "7": "256x144",
        #     "8": "176x144"
        # }
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
        self.th = None
        self.camera = neoapi.Cam()
        self.camera.Connect(self.cam_id)


    def get_image(self):
        input_frame = self.camera.GetImage()
        if not input_frame.IsEmpty():
            input_frame = input_frame.GetNPArray()
            # print(input_frame.shape)
            # input_frame = cv2.cvtColor(input_frame,cv2.COLOR_BAYER_RG2RGB)
            input_frame =cv2.resize(input_frame,(600, 600))
            # input_frame = 255 * np.ones((600,600,3), np.uint8)

            return input_frame

    def _start(self):
        while not self.killed:
            try:
                self.frame = self.get_image()
            except:
                continue
            if self.frame is None:
                continue
            # check tfms and apply here
            if self.part_id:
                print(f"part_id: {self.part_id}")
                self.tfms = self.get_tfms()
                if self.tfms:
                    print(self.tfms)
                    self.apply_transform()
            ####
            self.publish_frame_to_kafka()


class StreamBaumerFromRedis(Camera):

    def __init__(self,  KAFKA_BROKER_URL, topic, camera_id, width=400, height=400):
        self.width = width
        self.height = height
        self.resolutions = {
            "0": "1920x1080",
            "1": "1280x720",
            "2": "960x720",
            "3": "720x480",
            "4": "640x480",
            "5": "352x288",
            "6": "320x240",
            "7": "256x144",
            "8": "176x144"
        }
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
        self.redis_db = redis.StrictRedis(host="localhost", port=6379,db=0)
    

    def get_image(self):
        img1_bytes = self.redis_db.get('frames')
        decoded = cv2.imdecode(np.frombuffer(img1_bytes, np.uint8),1)
        decoded = cv2.resize(decoded, (self.height, self.width))
        return decoded

    def _start(self):
        print("In _start BaumerStreamClasss.................")
        while not self.killed:
            try:
                self.frame = self.get_image()
            except:
                continue
            if self.frame is None:
                print(f"frame is None")
                # print("None Frame Recieved!")
                continue
            # check tfms and apply here
            if self.part_id:
                self.tfms = self.get_tfms()
                if self.tfms:
                    print(self.tfms)
                    self.apply_transform()
            ####
            print("publishing to kafka")
            self.publish_frame_to_kafka()

def get_ip( device1):
        raw_ip = device1.nodemap['GevCurrentIPAddress'].value
        ip_list = []
        for i in range(4):
            temp = str(int(raw_ip/(256**(3-i))%256))
            ip_list.append(temp)
        # print(ip_list)
        final_ip = ip_list[0]+'.'+ip_list[1]+'.'+ip_list[2]+'.'+ip_list[3]
        # return {final_ip:raw_ip}
        return final_ip

class LUCID(Camera):
    def __init__(self, KAFKA_BROKER_URL, topic, camera_ips) -> None:
        super().__init__()
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_ips = camera_ips #Dictionary
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
        self.devices = None
        self.device = None
        self.frame = None
        self.killed = False

    def connect(self):
        tries = 0
        tries_max = 6
        sleep_time_secs = 10
        while tries < tries_max:  # Wait for device for 60 seconds
            self.devices = system.create_device()
            print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>",self.devices)
            if not self.devices:
                print(
                    f'Try {tries+1} of {tries_max}: waiting for {sleep_time_secs} '
                    f'secs for a device to be connected!')
                for sec_count in range(sleep_time_secs):
                    time.sleep(1)
                    print(f'{sec_count + 1 } seconds passed ',
                        '.' * sec_count, end='\r')
                tries += 1
            else:
                print(f'Created {len(self.devices)} device(s)\n')
                self.device = self.devices[0]
                break
        else:
            raise Exception(f'No device found! Please connect a device and run '
                            f'the example again.')

    def getImage(self):
        # camera_ips = {'221501115':'bottom'}#,'192.168.1.41':'top','192.168.1.35':'side'}
        self.camera_ips = {self.cam_ips:'cam'} #,'192.168.1.35':'side'
        cameras_dict = {}
        # for d in self.device:
        temp = get_ip(self.device)
        # temp = get_ser(d)
        cameras_dict[self.camera_ips[temp]] = self.device

        print(cameras_dict)


        initial_acquisition_mode_list = []

        for device in self.devices:
            print(device)
            nodemap = device.nodemap
            initial_acquisition_mode_list.append(nodemap.get_node("AcquisitionMode").value)
            nodemap.get_node("AcquisitionMode").value = "Continuous"
            tl_stream_nodemap = device.tl_stream_nodemap
            tl_stream_nodemap["StreamBufferHandlingMode"].value = "NewestOnly"
            tl_stream_nodemap['StreamAutoNegotiatePacketSize'].value = True
            tl_stream_nodemap['StreamPacketResendEnable'].value = True
            nodes = nodemap.get_node(['PixelFormat'])
            nodes['PixelFormat'].value = 'RGB8'

        while 1:
            for cam_pos, device in cameras_dict.items():
                with device.start_stream(1):

                    buffer = device.get_buffer()
                    item = BufferFactory.copy(buffer)
                    device.requeue_buffer(buffer)
                    npndarray = np.ctypeslib.as_array(item.pdata,shape=(item.height, item.width, 3)).reshape(item.height, item.width, 3)
                    npndarray = cv2.cvtColor(npndarray, cv2.COLOR_BGR2RGB)
                    npndarray = cv2.resize(npndarray,(1280,720))
                    # print(device.nodemap["GevCurrentIPAddress"].value)
                    # print((device.nodemap["GevCurrentIPAddress"].value).type)
                    # ip_dict = get_ip(device)
                    # cv2.imshow(device.nodemap["DeviceSerialNumber"].value, npndarray)
                    print(npndarray)
                    return npndarray

    def _start(self):
        while not self.killed:
            try:
                    self.frame = self.getImage()
            except Exception as e:
                    print(e)
                    continue
            if self.frame is None:
                print("None Frame Recieved!")
                continue
            #### check tfms and apply here
            # if self.part_id:
            #     self.tfms = self.get_tfms()
            #     if self.tfms:
            #         print(self.tfms)
            #         self.apply_transform()
            ####
            # print("About to publish")
            self.publish_frame_to_kafka()



class USBCamera(Camera):
    def __init__(self, KAFKA_BROKER_URL, topic, camera_id):
        self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.cam_id = camera_id
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
        self.th = None
        self.poll_for_part_id()
        self.cap = cv2.VideoCapture(int(self.cam_id))

    def _start(self):
        while not self.killed:
            try:
                ret, self.frame = self.cap.read()
            except:
                continue
            if self.frame is None:
                # print("None Frame Recieved!")
                continue
            # check tfms and apply here
            if self.part_id:
                self.tfms = self.get_tfms()
                if self.tfms:
                    print(self.tfms)
                    self.apply_transform()
            ####
            self.publish_frame_to_kafka()

def isNone(arg):
    if arg == None:
        return True
    return False

# def baumer(KAFKA_BROKER_URL, topic, ip):

#     ip = '192.168.0.1'

#     camera = neoapi.Cam()
#     camera.Connect(ip)
#     print(camera)
#     try:
#         input_frame = camera.GetImage().GetNPArray()
#         print(input_frame.shape)
#     except:
#         print("Frame not received")
#         return
#     input_frame = cv2.cvtColor(input_frame,cv2.COLOR_BAYER_RG2RGB)
#     input_frame =cv2.resize(input_frame,(1280, 720))

#     payload_video_frame = cv2.imencode('.jpeg', input_frame)[1].tobytes()
#         # print("Topic is")
#         # print(self.topic)
#     # print(payload_video_frame)
#     producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL)
#     future = producer.send(topic, value=payload_video_frame)
#     # report = future.get()

def ping_check(ip):

    cmd = ['ping', str(ip), '-c', '1']
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    # Linux Version p = subprocess.Popen(['ping','127.0.0.1','-c','1',"-W","2"])
    # The -c means that the ping will stop afer 1 package is replied
    # and the -W 2 is the timelimit
    if "Destination Host Unreachable" in output.decode('utf-8'):
        print("IP not connected")
        return False
    else:
        print("PING successful!!!!!!")
        return True



def run(ws_name, email):
    global domain
    global wids
    global camera_ids
    if ws_name == "" or email == "" :
        print("Please Enter all values")
        return
    #name = sys.argv[1]
    # print(name)
    # email = sys.argv[2]

    print(">>>>>>>>>>>",ws_name, email)
    # time.sleep(3)

    domain = tldextract.extract(email).domain
    print(domain)
    dict_of_topics = {}
    mp = MongoHelper().getCollection(WORKSTATION_COLLECTION, domain=domain)

    workstations = [p for p in mp.find({"$and": [{"isdeleted": False}, {"isdeleted": {"$exists": True}},
                                                 {"workstation_name": ws_name}]})]
    # print(WORKSTATION_COLLECTION, domain)
    print("Workstation FOUND : ", len(workstations))

    plc_flag = False
    plc_ip = None

    if workstations:
        for w_s in workstations:
            print(w_s)
            # PLC details retrieval from Mongo
            # plcs = w_s.get('plc', None)
            # if isNone(plcs):
            #     print("\nNo PLCs found")
            #     exit(1)
            # else:
            #     for plc in plcs:
            #         plc_ip = plc.get('plc_ip', None)
            #         if isNone(plc_ip):
            #             print("\nNO IP found for PLC")
            #             exit(1)
            #         else:
            #             for i in range(2):
            #                 if ping_check(plc_ip):
            #                     plc_flag = True
            #                     break
            #                 else:
            #                     print("\nPING was unsuccessful")
            #                     exit(1)

            cameras = w_s['cameras']
            wids = w_s['_id']
            for c in cameras:
                camera_ids = c['camera_id']
                topic = str(wids) + "_" + str(camera_ids) + "_i"
                topic = topic.replace(":", "-")
                dict_of_topics[topic] = c['camera_type']
                topic = ""
    # print(">>>>>>>>>>", dict_of_topics)
    # print(">>>>>>>>>>>>",topic, camera_id)
    for key, value in dict_of_topics.items():
        topic = key
        print(topic.split('_'))
        camera_id = str(topic.split('_')[1])
        value = "Gige - Baumer"
        if value == "IP":
            camera_id = camera_id.replace("-", ":")
            topic = topic.replace("-", "")
            topic = topic.replace(".", "")
            # print(topic)
            cc = IPWEBCAM(KAFKA_BROKER_URL, topic, camera_id)
        if value == "USB":
            print("&&&&&&&&&&&&&&&& usb topic")
            print(topic)
            cc = USBCamera(KAFKA_BROKER_URL, topic, camera_id)
        if value == "Gige - Baumer" or value == "Lucid":
            # cc = StreamBaumerFromRedis(KAFKA_BROKER_URL, topic, camera_id)
            # cc = Baumer(KAFKA_BROKER_URL, topic, camera_id)
            # cc.connect()
            cc = LUCID(KAFKA_BROKER_URL, topic, camera_id)
            cc.connect()
            # cc.getImage()
        print("Starting stream")
        cc.start()
        # t1 = threading.Thread(target=cc.start)
        # t1.start()

    # If plc_flag is True then PING is successful and plc_ip will have IP address of PLC
    inspection_id = ""
    process_flag = False
    reject_flag = False
    plc_flag = False
    # if plc_flag:
        # while True:
        #     from Button import Button
        #     process_running = Button("192.168.1.50", 24, 0, 1)
        #     sensor_trig = Button("192.168.1.50", 2, 0, 1)
        #     emergency_button = Button("192.168.1.50", 8, 1, 0)
        #     output_button = Button("192.168.1.50", 23, 2, 1) # 2- Rejected / idle value, 1- Accepted / active value

        #     # print(plc_ip)
        #     # cc.start()
        #     # time.sleep(1)
        #     process_trigger = process_running.get_status()
        #     # print("process",process_trigger)
        #     # print("sensor", sensor_trig.get_status() )
        #     # print("emergency",emergency_button.get_status())


        #     sensor_trig = sensor_trig.get_status()
        #     if process_trigger == 1 and process_flag == False:
        #         # time.sleep(2)
        #         print(">>>>>>>> Process trigger given")
                
        #         # time.sleep(2)
        #         if mode == "Inference":
        #             response, status_code = start_process_api(wids, part_id, part_number)
        #             if status_code == 200:
        #                 print("\nInspection Process Started!")
        #                 inspection_id = json.loads(response)["_id"]
        #                 print("Inspection Id: ", inspection_id)
        #             else:
        #                 print(response)
        #                 break
        #         process_flag = True

            
        #     if process_trigger == 0 and process_flag == True:
        #         print("\n>>>>>>>> Process stopped")
        #         if  mode == "Inference":
        #             end_process_api(inspection_id)
        #         process_flag = False
            
        #     if sensor_trig == 0 and reject_flag == True:
        #         reject_flag = False


        #     if sensor_trig == 1 and process_trigger == 1 and reject_flag == False:
        #     # if sensor_trig == 1 and process_trigger == 1 and sensor_flag == False:
        #         print("\n\n>>>>>>>>> Part found")
        #         time.sleep(3)
        #         if mode == "Inference":
        #             result = inspect_ui_trigger(camera_ids, wids, part_id )
                    

        #             # time.sleep(2)
        #             # result = True
        #             if result == True:
        #                 print("Part Accepted !")
        #                 output_button.write_value(1)
        #             else:
        #                 reject_flag = True
        #                 print("\nPART REJECTED!!!")
        #                 print("Remove part from conveyor and give Start Trigger again")
        #                 output_button.write_value(2)

        #         elif mode == "Train":
        #             time.sleep(1)
        #             result = save_images(camera_ids, wids, part_id)
                    
        #             if result["data"] == "success":
        #                 print("Image captured successfully!!!")
        #                 output_button.write_value(1)
        #             else:
        #                 save_images(camera_ids, wids, part_id)
        #                 output_button.write_value(1)

            
        #     emergency = emergency_button.get_status()
        #     if emergency == 0:
        #         process_flag = False

            # print(f"process_trigger: {process_running.get_status()}")
            # print(f"sensor_trigger: {sensor_trig.get_status()}")
            # print(f"emergency: {emergency_button.get_status()}")
            # time.sleep(0.1)
    # else:
    #     print("PLC connection not established")

# parser.add_argument('--ws', type=str,
#                      help='Workstation Name')


# parser.add_argument('--domain', type=str,
#                      help='AEmail registered in livis.ai')


# parser.add_argument('--part_id', type=str,
#                      help='Part ID')


# parser.add_argument('--part_name', type = str,
#                      help='Part Name')

# parser.add_argument('--mode', type = str,
#                      help='Mode of Running, Training or Inference')

# args = parser.parse_args()

# print(args.ws, args.domain, args.part_id, args.part_name, args.mode)
# print("TEST!@#$")
# run(args.ws, args.domain, args.part_id, args.part_name, args.mode)
#run("LINCODE-CONVEYOR", "divya.sai@livis.com", "6321b49217a52091f781f188", "kat-bolts", "Inference")
# end_process_api()
# save_images("0", "62171fd45d3e95fd7dff29e1","63184921420624e14b202778")1080

# def stream_kafka():
#     print(wids, camera_ids)
#     if wids == "":
#         return None
#     else:
#         topic = wids + "_" + camera_ids+ "_o"
#         consumer1 = KafkaConsumer(topic, bootstrap_servers=KAFKA_BROKER_URL, auto_offset_reset='latest', session_timeout_ms=3000)
#         for message in consumer1:
#             img_inference = cv2.imdecode(np.frombuffer(message.value, np.uint8), 1)
#         return img_inference
run("LINCODE-TEST001", "shreyas@livis.ai")
