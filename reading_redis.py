import redis
import cv2
import numpy as np 

r = redis.StrictRedis(host="localhost", port=6379,db=0)

while True:

    img1_bytes = r.get('frames')
    decoded = cv2.imdecode(np.frombuffer(img1_bytes, np.uint8),1)
    cv2.imshow('decoded_image', cv2.resize(decoded, (640,480)))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

