import urllib
import cv2
import numpy as np
import urllib.request
import time
import base64
import json
from kafka import KafkaProducer
import threading
from PIL import Image, ImageEnhance
import numpy
from pymongo import MongoClient
from abc import ABC, abstractmethod
import sys
import neoapi
import tldextract

camera = neoapi.Cam()

KAFKA_BROKER_URL = "3.6.88.61:9092"
MONGO_SERVER_HOST = "3.110.112.117"
MONGO_SERVER_PORT = 7778
MONGO_DB = "LIVIS"
WORKSTATION_COLLECTION = 'workstations'
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}


def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=MONGO_SERVER_HOST, port=MONGO_SERVER_PORT)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None, domain="lincode"):
        
        _DB = MONGO_DB
        DB = self.client[_DB]
        #make it as cname in [LIST OF COMMON COLLECTIONS]
        if cname == 'permissions':
            pass
        else:
            cname = domain + cname
        #print('collection_name for this request-------' ,cname )
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


## Parent Class
class Camera(ABC):
    #def __init__(self, KAFKA_BROKER_URL, topic, camera_id):
        # self.tfms = {}
        # self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        # self.topic = topic
        # self.cam_id = camera_id
        # self.frame = None
        # self.part_id = None
        # self.killed = False
        # self.thread = None
        # self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
        #                               value_serializer=lambda value: json.dumps(value).encode(), )
        # self.th = None
        # self.poll_for_part_id()
    def _poll_for_part_id(self):
        mp = MongoHelper().getCollection("cam_to_part", domain=domain)
        
        print("inside thisssss")
        # pr = mp.find_one({'camera_id' : int(self.cam_id)})
        if self.cam_id.find(":") != -1:
            pr = mp.find_one({'camera_id': self.cam_id})
        else:
            pr = mp.find_one({'camera_id': str(self.cam_id)})
        if pr:
            # print('found_part : : : ' , pr)
            self.part_id = pr['part_id']

    def poll_for_part_id(self):
        self.th = threading.Thread(target=self._poll_for_part_id)
        self.th.start()

    @abstractmethod
    def _start(self):
        pass

    def publish_frame_to_kafka(self):
        tmp_name = self.cam_id
        tmp_name = tmp_name.replace(":", "")
        tmp_name = tmp_name.replace("//", "")
        tmp_name = tmp_name.replace(".", "")
        f_name = str(tmp_name) + "_tmp.jpg"
        cv2.imwrite(f_name, self.frame)
        with open(f_name, 'rb') as f:
            im_b64 = base64.b64encode(f.read())
        payload_video_frame = {"frame": str(im_b64)}
        future = self.producer.send(self.topic, value=payload_video_frame)
        report = future.get()

    def get_tfms(self):
        self.tfms = {}
        if self.part_id:
            mp = MongoHelper().getCollection(self.part_id + "_preprocessingpolicy", domain=domain)
            pre = mp.find_one({'camera_id': str(self.cam_id)})
            p = pre['policy']
            for key, value in p.items():
                if key in ['brightness', 'contrast', 'hue', 'saturation']:
                    self.tfms[key] = value
                    # print(self.tfms[key])
        return self.tfms

    def apply_transform(self):
        if self.tfms:
            for k, val in self.tfms.items():
                val = float(val)
                print('applying tfms {}'.format(k))
                if k == 'brightness':
                    if val != 0:
                        val = val * 100
                        hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
                        h, s, v = cv2.split(hsv)
                        v = cv2.add(v, val)
                        v[v > 255] = 255
                        v[v < 0] = 0
                        final_hsv = cv2.merge((h, s, v))
                        self.frame = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
                    else:
                        self.frame = self.frame
                if k == 'contrast':
                    if val != 0:
                        val = val * 3
                        print("val is:", val)
                        ff = Image.fromarray(np.uint8(self.frame)).convert('RGB')
                        f = ImageEnhance.Contrast(ff)
                        e_img = f.enhance(val)
                        self.frame = numpy.asarray(e_img)
                    else:
                        self.frame = self.frame

    def start(self):
        self.thread = threading.Thread(target=self._start)
        self.thread.start()

    def stop(self):
        self.killed = True
        time.sleep(0.2)
        self.thread.join()
        self.thread = None
        self.killed = False
        
#class for Baumer Camera
class GIGICamera(Camera):
    def __init__(self, camera_ip,KAFKA_BROKER_URL,topic ):
        self.cam_id = camera_ip
        self.cam_id = str(self.cam_id).replace(":",".")
        #self.tfms = {}
        self.KAFKA_BROKER_URL = KAFKA_BROKER_URL
        self.topic = topic
        self.frame = None
        self.part_id = None
        self.killed = False
        self.thread = None
        self.producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
                                      value_serializer=lambda value: json.dumps(value).encode(), max_request_size= 419430400000000000000000,
                                      request_timeout_ms = 60000)
                                      #value_serializer=lambda value: json.dumps(value).encode())
        self.th = None
        #self.poll_for_part_id()
        self.conn = camera.Connect(str(self.cam_id))

    def publish_frame_to_kafka(self):
        print("inside publisher")
        tmp_name = self.cam_id
        tmp_name = tmp_name.replace(":", "")
        tmp_name = tmp_name.replace("//", "")
        tmp_name = tmp_name.replace(".", "")
        f_name = str(tmp_name) + "_tmp.jpg"
        cv2.imwrite(f_name, self.frame)
        with open(f_name, 'rb') as f:
            im_b64 = base64.b64encode(f.read())
        payload_video_frame = {"frame": str(im_b64)}
        # payload_video_frame = {"frame": str("i")}
        print("payyyyyyyyyyyyy")
        print(self.topic)
        print(self.producer)
        print(self.KAFKA_BROKER_URL )
        new_top = self.topic.replace(":","_")
        print(new_top)
        future = self.producer.send(str(new_top), value=payload_video_frame)
        print(future)
        report = future.get()
        print(report)

    # def get_image(self):
    #     # Get our image from the phone
        
    #     isColor = True
    #     if camera.f.PixelFormat.GetEnumValueList().IsReadable('BGR8'):
    #         camera.f.PixelFormat.SetString('BGR8')
    #     elif camera.f.PixelFormat.GetEnumValueList().IsReadable('Mono8'):
    #         camera.f.PixelFormat.SetString('Mono8')
    #         isColor = False
    #     else:
    #         print('no supported pixelformat')
    #         sys.exit(0)

    #     camera.f.AcquisitionFrameRateEnable.value = True
    #     camera.f.AcquisitionFrameRate.value = 10
    #     imgNp = camera.GetImage().GetNPArray()
    #     print(imgNp)
    #     # Convert our image again but this time to opencv format
    #     #img = cv2.imdecode(imgNp,-1)
    #     return imgNp
        
    def _start(self):
        while True:
            print("Accessing frame")
            isColor = True
            self.frame = camera.GetImage().GetNPArray()
                    
            if self.frame is [] or self.frame is None or len(self.frame) == 0:
                print("yrwee")
                continue
            else:
                #print(self.frame)
                # self.frame = cv2.resize(self.frame,(1920,1080))
                self.publish_frame_to_kafka()



global domain
ws_name = sys.argv[1]
email = sys.argv[2]
domain = tldextract.extract(email).domain

new_list = []
new_list2 = []


mp = MongoHelper().getCollection('workstations', domain=domain)
coll = mp.find_one({"workstation_name": str(ws_name)})
#print(coll)
if coll is None:
    print("INVALID WORSTATION NAME :::::::::::")
    sys.exit(0)
cams = coll['cameras']
for i in cams:
    if i['camera_type'] == 'GIGE-BAUMER':
        new_ip = i['camera_id'].replace(".",":")
        new_list.append(new_ip)
        new_list2.append(i['camera_id'])

wid = coll["_id"]

base_topic = str(wid)
print("\ntopic "+base_topic)

print("\n new list is"+str(new_list))

for i in new_list:
    topic = str(wid) + '_' + str(i) + "_i"
    topic = topic.replace(":","")
    cc = GIGICamera(camera_ip=i,KAFKA_BROKER_URL=KAFKA_BROKER_URL, topic=topic)
    cc.start()
